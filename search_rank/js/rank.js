function rank(tag_list, tag_url, tag_page,tag_tip) {
    var links = [];
    try {
        

        chrome.storage.sync.get('url', function(res) {
            var links=[];
            if(res.url){
                links = res.url.split(/[\n]/);
            }
            
            var list = $(tag_list);
            var tip=[];
            console.log(list.length);
            if (list.length > 0) {
                list.each(function(index, item) {
                    var pos = 0;
                    vo = $(this);
                    if (document.location.toString().indexOf("www.baidu.com/") != -1) {
                        if (!vo.attr('style')) {
                            pos = parseInt(vo.attr('id'));
                        }
                    } else {
                        pos = (parseInt($(tag_page).text()) - 1) * 10 + index + 1;
                    }

                    var h3 = vo.find("h3").first();
                    if ($("#rank-" + pos + "").length == 0 && pos > 0) {
                        var url = vo.find(tag_url).text();
                        var back = "#99CCFF";
                        for (i in links) {
                            if (links[i]!="" && url.indexOf(links[i]) > -1) {
                                back = "red";
                                if(!tip[i]){
                                    tip[i]="<div>"+links[i]+" 排名位置：<strong style='color:red; ' >"+pos+"</strong></div>";
                                }
                                vo.find(tag_url).css('color', 'red');
                                console.log(vo.find("a").attr("href"));
                                vo.find("a").click();
                            }
                        }

                        h3.before("<span  id='rank-" + pos +
                            "' style='position: absolute; margin-left:-60px;border-radius: 6px;width: 50px;display: inline-block;margin-right: 8px;box-sizing:border-box;color: #fff;  background:" +
                            back + "; padding: 10px 0;text-align: center;font-size:24px;'>" +
                            pos + " </span>");

                    }

                });
               
                
                if(tip.length>0){
                    $(tag_tip).prepend("<div style='border-radius:6px; background:#fcf8e3;padding:5px 10px;margin-bottom:10px;' >"+tip.join("")+"</div>")
                };
               
            }
        });
    } catch (err) {
        console.log(err);
    }
}


if (document.location.toString().indexOf("www.baidu.com/") != -1) {

    $("head").on('DOMNodeInserted', function() {
        rank("#content_left div.c-container", ".c-showurl", "#page strong .pc","#content_left");
    });

}
if (document.location.toString().indexOf("www.sogou.com/") != -1) {
    $("head").on('DOMNodeInserted', function() {
        rank(".results>div", ".fb cite", "#pagebar_container span","#promotion_adv_container");
    });
}
if (document.location.toString().indexOf("www.so.com/") != -1) {
    $("head").on('DOMNodeInserted', function() {
        rank(".result>.res-list", ".g-linkinfo cite", "#page strong","#m-spread-left");
    });
}
