# 查排名

#### 介绍
查排名支持百度、搜狗、360搜索，查询关键词排名位置的浏览器插件，支持基于chrome的浏览器

 
#### 安装教程

1.  第一步 [下载安装包](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-59cbc039-57ea-4575-9db0-1921d7ea41a2/808d8762-6101-4533-9a1d-afb46a35f243.crx)
2.  第二步 打开扩展管理页面,浏览器输入地址“ chrome://extensions/ ”进入扩展程序页面，开启开发者模式
3.  第三步 将下载的saerch_rank.crx文件拖拽到“chrome://extension”页面, 完成安装

